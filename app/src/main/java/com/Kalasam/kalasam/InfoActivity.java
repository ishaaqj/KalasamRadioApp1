package com.Kalasam.kalasam;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

public class InfoActivity extends AppCompatActivity {
    /* onCreate(Bundle savedInstanceState) - Method that creates the interface that the user uses
     * Bundle savedInstanceState - variable that holds the user interface */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
    }
}
