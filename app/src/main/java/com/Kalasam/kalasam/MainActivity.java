package com.Kalasam.kalasam;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    /* STREAM_URL - Holds the URL used to access the Kalasam Radio Stream */
    private String STREAM_URL = "http://173.192.207.51:8084/;stream.mp3";

    /* mPlayer - Object of MediaPlayer Class. Will be used to connect to the stream and play music */
    private MediaPlayer mPlayer;

    /*
    volumeSeekbar - Object of Seekbar class. Will be used to change the thumb of the seekbar
    and allow the seekbar to change the systems volume
    */
    private SeekBar volumeSeekbar = null;

    /* audioManager - Object of AudioManager class. Will be used to play music from the stream */
    private AudioManager audioManager = null;


    @Override
    /* onCreate(Bundle savedInstanceState) - Method that creates the interface that the user uses
     * Bundle savedInstanceState - variable that holds the user interface */
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setVolumeControlStream(AudioManager.STREAM_MUSIC);
        setContentView(R.layout.activity_main);
        mPlayer = new MediaPlayer();
    }

    @Override
    protected void onStart() {
        super.onStart();
        initControls();
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
    }

    /* sendEmail() - Method that will create a email pop-up and allow the user to send an email to
     *               Kalasam.com directly */
    protected void sendEmail() {
        Log.i("Send email", "");
        String[] TO = {"info@kalasam.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            Log.i("Finished sending email...", "");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    /* initControls()-This method manipulates the seekbar. It is here that we change the thumb of the seekbar
     *                and also allow the seekbar to change the volume of the system */
    private void initControls()
    {
        try
        {
            volumeSeekbar = (SeekBar)findViewById(R.id.seekBar);
            audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            ShapeDrawable thumb = new ShapeDrawable( new OvalShape());
            thumb.setIntrinsicHeight(50);
            thumb.setIntrinsicWidth(50);
            volumeSeekbar.setThumb(thumb);
            volumeSeekbar.setMax(audioManager
                    .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
            volumeSeekbar.setProgress(audioManager
                    .getStreamVolume(AudioManager.STREAM_MUSIC));

            volumeSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
            {
                @Override
                public void onStopTrackingTouch(SeekBar arg0)
                {
                }

                @Override
                public void onStartTrackingTouch(SeekBar arg0)
                {
                }

                @Override
                public void onProgressChanged(SeekBar arg0, int progress, boolean arg2)
                {
                    audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                            progress, 0);
                }
            });
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /* emailButtonClick - method that allows the email button to access the sendEmail() method*/
    public void emailButtonClick(View view){
        sendEmail();
    }

    /* playButtonClick(View view) - Method that allows the play button to start listening to the kalasam stream
    *                               and start playing the music
    *  View view - variable that allows the programmer to get access to buttons on the xml main_activity layout*/
    public void playButtonClick(View view){
       ImageButton playButton = (ImageButton) findViewById(R.id.play_button);
       ImageButton stopButton = (ImageButton) findViewById(R.id.cancel_button);
        if (!mPlayer.isPlaying()) {
            try {
                playButton.setVisibility(View.INVISIBLE);
                playButton.setClickable(false);
                stopButton.setVisibility(View.VISIBLE);
                stopButton.setClickable(true);
                mPlayer.reset();
                Uri uri = Uri.parse(STREAM_URL);
                Map<String, String> headerMap = new HashMap<String, String>();;
                headerMap.put("User-Agent", "Kalasam Radio Official App");
                mPlayer.setDataSource(view.getContext(), uri, headerMap);
                mPlayer.prepareAsync();
                mPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        mp.start();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /* stopButtonClick(View view) - Method that allows the stop button to stop playing the kalasam stream
    *  View view - variable that allows the programmer to get access to buttons on the xml main_activity layout*/
    public void stopButtonClick(View view){
       ImageButton playButton = (ImageButton) findViewById(R.id.play_button);
       ImageButton stopButton = (ImageButton) findViewById(R.id.cancel_button);
       stopButton.setVisibility(View.INVISIBLE);
        stopButton.setClickable(false);
       playButton.setVisibility(View.VISIBLE);
        playButton.setClickable(true);
        mPlayer.stop();
    }

    /* infoButtonClick(View view) - Connected to the about button. When clicked, it will open a new about page that gives details
     *                   on the projects and the development team
     * View view - variable that allows the programmer to get access to buttons on the xml main_activity layout*/
    public void infoButtonClick(View view){
        startActivity(new Intent(this, InfoActivity.class));
    }

    /* websiteButtonClick(View view) - Connected to the website button. When clicked it will navigate the user to the Kalasam.com page
     * View view - variable that allows the programmer to get access to buttons on the xml main_activity layout */
    public void websiteButtonClick(View view){
        Uri uri = Uri.parse("http://www.kalasam.com/Tamil-radio-tamil-fm-news/Tamilradio-tamilfmradio-contact.htm"); // missing 'http://' will cause crashed
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }

}
